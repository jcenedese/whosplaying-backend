package com.jcenedese.whosplaying.model;

import java.io.Serializable;

public class ChampionDto implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6704131245290182612L;
	/**
	 * Id du champion
	 */
	private int id;
	/**
	 * Icone du champion
	 */
	private byte[] icon;

	/**
	 * Nom du champion
	 */
	private String name;
	
	/**
	 * Titre du champion
	 */
	private String title;
	
	/**
	 * Constructeur par défaut
	 */
	public ChampionDto() {
		super();
	}
	
	/**
	 * Constructeur par paramètres
	 * @param id id du champion
	 * @param icon icone du champion
	 * @param name nom du champion
	 */
	public ChampionDto(int id, String name, String title, byte[] icon) {
		super();
		this.id = id;
		this.name = name;
		this.title = title;
		this.icon = icon;
	}
	
	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the icon
	 */
	public byte[] getIcon() {
		return icon;
	}

	/**
	 * @param icon the icon to set
	 */
	public void setIcon(byte[] icon) {
		this.icon = icon;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
}

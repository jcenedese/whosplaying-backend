package com.jcenedese.whosplaying.model;

import java.io.Serializable;

import net.rithms.riot.api.endpoints.summoner.dto.Summoner;

public class PlayerDto implements Serializable{

	/**
	 * Serial généré
	 */
	private static final long serialVersionUID = -4379131391019343215L;
	
	/**
	 * id du compte
	 */
	private String accountId;
	
	/**
	 * Id du joueur
	 */
	private String id;
	
	/**
	 * Pseudo du joueur
	 */
	private String name;
	
	/**
	 * Champion associé
	 */
	private Long champion;
	
	/**
	 * Taux de victoire, en %
	 */
	private Float winrate;
	
	/**
	 * Indique si le joueur joue son champion favori
	 */
	private Boolean playingFavoriteChampion;
	
	/**
	 * Statut de la partie en cours
	 */
	private GameInformation currentGame;
	
	/**
	 * Constructeur par défaut
	 */
	public PlayerDto(){
		
	}
	
	/**
	 * Constructeur par paramètres
	 * @param summoner {@link Summoner} tel que défini par Riot Games
	 * @param championId id du champion joué
	 */
	public PlayerDto(Summoner summoner, Long championId, Float winrate){
		this.accountId = summoner.getAccountId();
		this.id = summoner.getId();
		this.name = summoner.getName();
		this.champion = championId;
		this.winrate = winrate;
	}
	

	/**
	 * @return the accountId
	 */
	public String getAccountId() {
		return accountId;
	}
	
	/**
	 * @param accountId the accountId to set
	 */
	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the rank
	 */
	public Float getWinrate() {
		return winrate;
	}

	/**
	 * @param rank the rank to set
	 */
	public void setWinrate(Float winrate) {
		this.winrate = winrate;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * @return the champion
	 */
	public Long getChampion() {
		return champion;
	}
	
	/**
	 * @param champion the champion to set
	 */
	public void setChampion(Long champion) {
		this.champion = champion;
	}

	/**
	 * @return the currentGame
	 */
	public GameInformation getCurrentGame() {
		return currentGame;
	}

	/**
	 * @param currentGame the currentGame to set
	 */
	public void setCurrentGame(GameInformation currentGame) {
		this.currentGame = currentGame;
	}

	/**
	 * @return the playingFavoriteChampion
	 */
	public Boolean getPlayingFavoriteChampion() {
		return playingFavoriteChampion;
	}

	/**
	 * @param playingFavoriteChampion the playingFavoriteChampion to set
	 */
	public void setPlayingFavoriteChampion(Boolean playingFavoriteChampion) {
		this.playingFavoriteChampion = playingFavoriteChampion;
	}
}

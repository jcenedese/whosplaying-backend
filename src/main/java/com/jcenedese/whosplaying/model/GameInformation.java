package com.jcenedese.whosplaying.model;

import net.rithms.riot.api.endpoints.spectator.dto.CurrentGameInfo;

public class GameInformation {
	
	private Long gameLength;
	
	private String spectatorKey;
	
	private String gameMode;
	
	private Boolean playingFavoriteChampion;
	
	private Long gameId;
	
	private String platformId;
	
	public GameInformation() {
		
	}
	
	public GameInformation(PlayerDto player, CurrentGameInfo currentGameInfo) {
		if(currentGameInfo != null) {
			this.gameLength = currentGameInfo.getGameLength();
			this.spectatorKey = currentGameInfo != null ? currentGameInfo.getObservers().getEncryptionKey() : null;
			this.gameMode = currentGameInfo.getGameMode();
			this.playingFavoriteChampion = currentGameInfo.getParticipantByParticipantId(player.getId()).getChampionId() == player.getChampion().intValue();
			this.setGameId(currentGameInfo.getGameId());
			this.setPlatformId(currentGameInfo.getPlatformId());
		}
	}

	/**
	 * @return the gameLength
	 */
	public long getGameLength() {
		return gameLength;
	}

	/**
	 * @param gameLength the gameLength to set
	 */
	public void setGameLength(long gameLength) {
		this.gameLength = gameLength;
	}

	/**
	 * @return the spectatorKey
	 */
	public String getSpectatorKey() {
		return spectatorKey;
	}

	/**
	 * @param spectatorKey the spectatorKey to set
	 */
	public void setSpectatorKey(String spectatorKey) {
		this.spectatorKey = spectatorKey;
	}

	/**
	 * @return the gameMode
	 */
	public String getGameMode() {
		return gameMode;
	}

	/**
	 * @param gameMode the gameMode to set
	 */
	public void setGameMode(String gameMode) {
		this.gameMode = gameMode;
	}

	/**
	 * @return the playingFavoriteChampion
	 */
	public boolean isPlayingFavoriteChampion() {
		return playingFavoriteChampion;
	}

	/**
	 * @param playingFavoriteChampion the playingFavoriteChampion to set
	 */
	public void setPlayingFavoriteChampion(boolean playingFavoriteChampion) {
		this.playingFavoriteChampion = playingFavoriteChampion;
	}

	/**
	 * @return the gameId
	 */
	public Long getGameId() {
		return gameId;
	}

	/**
	 * @param gameId the gameId to set
	 */
	public void setGameId(Long gameId) {
		this.gameId = gameId;
	}

	/**
	 * @return the platformId
	 */
	public String getPlatformId() {
		return platformId;
	}

	/**
	 * @param platformId the platformId to set
	 */
	public void setPlatformId(String platformId) {
		this.platformId = platformId;
	}
	
	

}

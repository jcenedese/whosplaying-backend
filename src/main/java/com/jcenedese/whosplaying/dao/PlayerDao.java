package com.jcenedese.whosplaying.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Repository;

import com.jcenedese.whosplaying.model.PlayerDto;
import com.jcenedese.whosplaying.utils.Constants;

@Repository
/**
 * TODO: utiliser hibernate/JPA/des entités
 * 
 * @author jcenedes
 *
 */
public class PlayerDao {
	
	private static String GET_PLAYERS_BY_CHAMPION_ID_STATEMENT = "SELECT p.summoner_id, p.name, p.account_id, p.champion_id, p.winrate"
			+ " FROM Player p"
			+ " WHERE p.champion_id=?"
			+ " AND p.server=?";
	
	private static String INSERT_PLAYERS_STATEMENT = "INSERT INTO PLAYER(summoner_id,name,account_id,champion_id, winrate, server) VALUES(?,?,?,?,?,?)";
	
	private static String INSERT_ADDITIONAL_PLAYER = ", (?,?,?,?,?,?)";
	
	
	/**
	 * Récupère les joueurs à partir de l'id du champion
	 * @param championId l'id du champion
	 * @return le joueur
	 */
	public List<PlayerDto> getPlayersByChampionId(Long championId){
		List<PlayerDto> PlayerDtoList = new ArrayList<PlayerDto>();
		if(championId != null){
	    	try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL); PreparedStatement  stmt = conn.prepareStatement(GET_PLAYERS_BY_CHAMPION_ID_STATEMENT)) {
	            if (conn != null) {
	        		stmt.setLong(1, championId);
	    			stmt.setString(2, Constants.GAME_SERVER.getId());
	    			 ResultSet rs = stmt.executeQuery();
	                 while (rs.next()) {
	                	 PlayerDto playerDto = new PlayerDto();
	                	 playerDto.setId(rs.getString("summoner_id"));
	                	 playerDto.setName(rs.getString("name"));
	                	 playerDto.setAccountId(rs.getString("account_id"));
	                	 playerDto.setChampion(rs.getLong("champion_id"));
	                	 playerDto.setWinrate(rs.getFloat("winrate"));
	                	 PlayerDtoList.add(playerDto);
	                 }
	            }
	        } catch (SQLException e) {
				e.printStackTrace();
			}
		}

	System.out.println(PlayerDtoList.size() + " joueurs trouvés en base");
	return PlayerDtoList;
	}
	
	/**
	 * Enregistre les joueurs dans la base de données
	 * @param players la {@link List} de {@link PlayerDto}
	 */
	public void savePlayers(List<PlayerDto> players){
    	if(players != null){
    		
	    		StringBuilder queryString = new StringBuilder(INSERT_PLAYERS_STATEMENT);
	    		for(int i = 1; i < players.size(); i++) {
	    			queryString.append(INSERT_ADDITIONAL_PLAYER);
	    		}
	    		try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL); PreparedStatement  stmt = conn.prepareStatement(queryString.toString())) {
	                if (conn != null) {
	                	IntStream.range(0, players.size()).forEach(index -> {
	                		PlayerDto player = players.get(index);
	    					int paramId = index * 6;
	    					
	    					try {
	    						stmt.setString(++paramId, player.getId());
				    			stmt.setString(++paramId, player.getName());
				    			stmt.setString(++paramId, player.getAccountId());
			    				stmt.setLong(++paramId, player.getChampion());
			    				stmt.setFloat(++paramId, player.getWinrate());
				    			stmt.setString(++paramId, Constants.GAME_SERVER.getId());
	    					} catch (SQLException e) {
	    						e.printStackTrace();
	    					}
	    				});
	    			stmt.executeUpdate();
	                }
	                System.out.println(players.size() + " lignes insérées");
	            } catch (SQLException e) {
					e.printStackTrace();
				}
    	}
		
    }
}

package com.jcenedese.whosplaying.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import org.springframework.stereotype.Repository;

import com.jcenedese.whosplaying.utils.Constants;


@Repository
public class SchemaBuilder {
	
    
    public static void createNewDatabase() {
        // SQL statement for creating a new table
        String createChampTableSql = "CREATE TABLE IF NOT EXISTS CHAMPION ("
                + "	id integer PRIMARY KEY,"
                + "	name text NOT NULL,"
                + " icon blob,"
                + " title text"
                + ");";
        
        String createPlayerTableSql = "CREATE TABLE IF NOT EXISTS PLAYER ("
                + "	summoner_id text NOT NULL,"
                + "	account_id text NOT NULL,"
                + "	name text NOT NULL,"
                + "	rank text,"
                + " winrate real,"
                + "	champion_id integer,"
                + "	server text NOT NULL,"
                + "	PRIMARY KEY (summoner_id, champion_id)"
                + ");";
 
        try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL); Statement stmt = conn.createStatement()) {
            if (conn != null) {
                stmt.execute(createChampTableSql);
                System.out.println("table des champions créée!");
                stmt.execute(createPlayerTableSql);
                System.out.println("table des joueurs créée!");
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
    }
}

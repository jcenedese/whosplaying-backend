package com.jcenedese.whosplaying.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

import org.springframework.stereotype.Repository;

import com.jcenedese.whosplaying.model.ChampionDto;
import com.jcenedese.whosplaying.utils.Constants;
import net.rithms.riot.constant.Platform;

@Repository
/**
 * TODO: utiliser hibernate/JPA/des entités
 * 
 * @author jcenedes
 *
 */
public class ChampionDao {

	private static String INSERT_STATEMENT = "INSERT INTO CHAMPION(id, name, title, icon) VALUES(?,?,?,?)";
	
	private static String INSERT_ADDITIONAL_CHAMP = ", (?,?,?,?)";

	private static String GET_ALL_STATEMENT = "SELECT id, name, title, icon FROM CHAMPION";
	
	private static String GET_BY_ID_STATEMENT = "SELECT c.id, c.name, c.title, c.icon FROM CHAMPION c "
			+ "WHERE c.id=?";

	/**
	 * Insère les champions pour une région donnée
	 * 
	 * @param championList la liste des {@link ChampionDto} à insérer
	 * @param server       la {@link Platform} concernée
	 */
	public void saveChampions(List<ChampionDto> championList) {
		
		StringBuilder queryString = new StringBuilder(INSERT_STATEMENT);
		for(int i = 1; i < championList.size(); i++) {;
			queryString.append(INSERT_ADDITIONAL_CHAMP);
		}
		System.out.println(queryString.toString());

		try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL); PreparedStatement stmt = conn.prepareStatement(queryString.toString())) {;
			if (conn != null) {
				IntStream.range(0, championList.size()).forEach(index -> {
					ChampionDto champion = championList.get(index);
					int paramId = index * 4;
					
					try {
						stmt.setInt(++paramId, champion.getId());
						stmt.setString(++paramId, champion.getName());
						stmt.setString(++paramId, champion.getTitle());
						stmt.setBytes(++paramId, champion.getIcon());
					} catch (SQLException e) {
						e.printStackTrace();
					}
				});
				
				stmt.executeUpdate();
				System.out.println("La table des champions contient désormais " + championList.size() + " éléments." );
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Retourne la liste des champions
	 * 
	 * @return la liste des {@link ChampionDto} actuellement en base
	 */
	public List<ChampionDto> getChampions() {
		
		List<ChampionDto> champions = new ArrayList<ChampionDto>();
		try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL);
				Statement stmt = conn.createStatement()) {
			if (conn != null) {
				ResultSet rs = stmt.executeQuery(GET_ALL_STATEMENT);
				while (rs.next()) {
					champions.add(new ChampionDto(rs.getInt("id"), rs.getString("name") , rs.getString("title"), rs.getBytes("icon")));
				}
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return champions;
	}
	
	/**
	 * Retourne la liste des champions
	 * 
	 * @return la liste des {@link ChampionDto} actuellement en base
	 */
	public ChampionDto getChampion(Integer championId) {
		
		ChampionDto champion = new ChampionDto();
		try (Connection conn = DriverManager.getConnection(Constants.DATABASE_URL); PreparedStatement stmt = conn.prepareStatement(GET_BY_ID_STATEMENT)) {
			if (conn != null) {
				stmt.setInt(1, championId);
				ResultSet rs = stmt.executeQuery();
				rs.next();
				champion = new ChampionDto(rs.getInt("id"), rs.getString("name") , rs.getString("title"), rs.getBytes("icon"));
			}
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return champion;
	}
}

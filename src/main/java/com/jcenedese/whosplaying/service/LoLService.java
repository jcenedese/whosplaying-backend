package com.jcenedese.whosplaying.service;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.jcenedese.whosplaying.utils.Constants;
import com.jcenedese.whosplaying.model.ChampionDto;
import com.jcenedese.whosplaying.model.GameInformation;
import com.jcenedese.whosplaying.model.PlayerDto;
import net.rithms.riot.api.ApiConfig;
import net.rithms.riot.api.RiotApi;
import net.rithms.riot.api.RiotApiException;
import net.rithms.riot.api.endpoints.spectator.dto.CurrentGameInfo;
import net.rithms.riot.constant.Platform;

/**
 * Service en charge des appeles à l'API de Riot Games
 * 
 * @author jcenedes
 *
 */
@Service
public class LoLService {

	/**
	 * L'API de Riot
	 */
	private RiotApi api = new RiotApi(new ApiConfig().setKey(Constants.API_KEY));

	private static final String DATA_DRAGON_HOST = "https://ddragon.leagueoflegends.com";
	private static final String DATA_DRAGON_REALM_VERSIONS = "/realms/euw.json";
	private static final String DATA_DRAGON_CHAMPIONS_DATA = "/cdn/%s/data/fr_FR/champion.json";
	private static final String DATA_DRAGON_CHAMPION_ICON = "/cdn/%s/img/champion/%s.png";

	/**
	 * Récupère tous les champions du jeu
	 * 
	 * @return
	 */
	public List<ChampionDto> getAllChampions() {
		String championApiVersion = getChampionApiVersion();
		JsonObject rootElement = this
				.getRootObjectFromUrl(DATA_DRAGON_HOST + String.format(DATA_DRAGON_CHAMPIONS_DATA, championApiVersion));
		List<ChampionDto> championList = new ArrayList<>();
		if (rootElement != null) {
			championList = rootElement.get("data").getAsJsonObject().entrySet().stream().map((entry) -> {
				ChampionDto champion = new ChampionDto();
				champion.setName(entry.getKey());
				champion.setId(Integer.valueOf(entry.getValue().getAsJsonObject().get("key").getAsString()));
				champion.setTitle(entry.getValue().getAsJsonObject().get("title").getAsString());
				champion.setIcon(this.getIconByChampionName(championApiVersion, entry.getKey()));
				return champion;
			}).collect(Collectors.toList());
		}

		return championList;
	}

	private byte[] getIconByChampionName(String apiVersion, String championName) {
		byte[] iconBytes = null;
		if (!StringUtils.isEmpty(apiVersion) && !StringUtils.isEmpty(championName)) {
			URL url = null;
			try {
				url = new URL(DATA_DRAGON_HOST + String.format(DATA_DRAGON_CHAMPION_ICON, apiVersion, championName));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			if (url != null) {
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				try (InputStream inputStream = url.openStream()) {
					int n = 0;
					byte[] buffer = new byte[1024];
					while (-1 != (n = inputStream.read(buffer))) {
						output.write(buffer, 0, n);
					}
					iconBytes = output.toByteArray();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return iconBytes; //
	}

	private String getChampionApiVersion() {
		JsonObject rootElement = this.getRootObjectFromUrl(DATA_DRAGON_HOST + DATA_DRAGON_REALM_VERSIONS);
		String version = "";
		if (rootElement != null) {
			version = rootElement.get("n").getAsJsonObject().get("champion").getAsString();
		}
		return version;
	}

	private JsonObject getRootObjectFromUrl(String url) {
		JsonObject rootObject = null;
		try {
			URLConnection request = new URL(url).openConnection();
			request.connect();
			JsonElement root = new JsonParser()
					.parse(new InputStreamReader((InputStream) request.getContent(), StandardCharsets.UTF_8));
			rootObject = root.getAsJsonObject();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return rootObject;
	}

	/**
	 * Récupère les joueurs à partir de leur nom depuis l'API de Riot
	 * 
	 * @param playerNames les noms des joueurs
	 * @param championId  l'id du champion qu'ils jouent
	 */
	public List<PlayerDto> getSummonersByPlayerNameList(Map<String, Float> playerStats, Long championId) {
		return playerStats.entrySet().stream().map((entry) -> {
			try {
				return new PlayerDto(this.api.getSummonerByName(Constants.GAME_SERVER, entry.getKey()), championId,
						entry.getValue());
			} catch (RiotApiException e) {
				System.err.println(entry.getKey() + " introuvable");
				e.printStackTrace();
				return null;
			}
		}).filter(Objects::nonNull).collect(Collectors.toList());
	}

	/**
	 * Renseigne, pour chacun des joueurs passés en paramètre, les informations sur
	 * la partie en cours
	 * 
	 * @param players
	 */
	public void checkInGameStatus(List<PlayerDto> players) {
		if (players != null) {

			if (players.size() > Constants.MAX_SUMMONER_SIZE) {
				players = players.subList(0, Constants.MAX_SUMMONER_SIZE);
			}

			int count = 0;
			for (int i = 0; i < players.size() && count < Constants.MAX_SUMMONER_SIZE; i++) {
				PlayerDto player = players.get(i);
				try {
					CurrentGameInfo gameInfo = this.api.getActiveGameBySummoner(Platform.EUW, player.getId());
					if (gameInfo != null) {
						player.setCurrentGame(new GameInformation(player, gameInfo));
						System.out.println(player.getName() + " EN JEU DEPUIS : " + gameInfo.getGameLength() / 60
								+ " minutes et + " + gameInfo.getGameLength() % 60 + " secondes.");
						count++;
					}
				} catch (RiotApiException e) {
					System.out.println("pas en jeu");
				}
			}
		}
	}

	public byte[] getWatchFileForGameId(Long gameId, String platformId, String spectateKey) {
		byte[] result = null;

		String content;
		try {
			Charset charset = StandardCharsets.UTF_8;
			content = this.readFromInputStream(getClass().getClassLoader().getResourceAsStream("spectator.bat"));
			content = content.replace("%observer_url%", Constants.OBSERVER_URL);
			content = content.replace("%encryption_key%", spectateKey);
			content = content.replace("%platform_id%", platformId);
			content = content.replace("%game_id%", String.valueOf(gameId));
			result = content.getBytes(charset);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}
	
	
	private String readFromInputStream(InputStream inputStream)
			  throws IOException {
			    StringBuilder resultStringBuilder = new StringBuilder();
			    try (BufferedReader br
			      = new BufferedReader(new InputStreamReader(inputStream))) {
			        String line;
			        while ((line = br.readLine()) != null) {
			            resultStringBuilder.append(line).append("\n");
			        }
			    }
			  return resultStringBuilder.toString();
			}

}

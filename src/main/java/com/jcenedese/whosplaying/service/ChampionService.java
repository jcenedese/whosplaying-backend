package com.jcenedese.whosplaying.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jcenedese.whosplaying.dao.ChampionDao;
import com.jcenedese.whosplaying.model.ChampionDto;


/**
 * Service des champions.
 */
@Service
public class ChampionService {
	
	@Autowired
	ChampionDao championDao;
	
	@Autowired
	LoLService lolService;
	
	
	/**
	 * Initialise la liste des champions
	 * @return
	 */
	public List<ChampionDto> initChampions() {
		List<ChampionDto> championList = lolService.getAllChampions();
		this.championDao.saveChampions(championList);
		return championList;
	}
	
	/**
	 * Récupère tous les champions et initialise la liste si nécessaire
	 * @return une liste de {@link ChampionDto}
	 */
	public List<ChampionDto> getAllChampions(){
		List<ChampionDto> championList = this.championDao.getChampions();
		if(championList.isEmpty()) {
			System.out.println("Liste des champions vides, on l'initialise");
			championList = this.initChampions();
		}
		return championList;
	}	
	
	/**
	 * Récupère un champion à partir de son id
	 * @return un {@link ChampionDto}
	 */
	public ChampionDto getChampion(Integer championId){
		if(championId == null) {
			return null;
		}
		return this.championDao.getChampion(championId);
	}	
}

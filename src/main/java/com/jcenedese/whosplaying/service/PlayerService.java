package com.jcenedese.whosplaying.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jcenedese.whosplaying.dao.PlayerDao;
import com.jcenedese.whosplaying.model.PlayerDto;

@Service
public class PlayerService {
	
	@Autowired
	private PlayerDao playerDao;
	
	@Autowired
	private ScrappingService scrappingService;
	
	@Autowired
	private LoLService lolService;
	
	public List<PlayerDto> getBestPlayersByChampionId(Long championId){
		List<PlayerDto> players = new ArrayList<>();
		players.addAll(this.playerDao.getPlayersByChampionId(championId));
		
		if(players.isEmpty()) {
			Map<String,Float> mostActiveSummoners = this.scrappingService.getMostActiveSummonerNamesByChampionId(championId);
			players.addAll(this.lolService.getSummonersByPlayerNameList(mostActiveSummoners, championId));
			this.playerDao.savePlayers(players);
		}
		
		this.lolService.checkInGameStatus(players);
		
		return players;
	}

}

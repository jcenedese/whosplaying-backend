package com.jcenedese.whosplaying.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.springframework.stereotype.Service;

import com.jcenedese.whosplaying.utils.Constants;

@Service
public class ScrappingService {
	
	/**
	 * Retourne une map contenant les noms d'invocateur et leur winrate associé
	 * @param championId l'id du champion
	 * @return une {@link Map} de {@link String},{@link Float}
	 */
	public Map<String, Float> getMostActiveSummonerNamesByChampionId(Long championId){
		Document doc;
		Map<String, Float> summoners = new HashMap<String,Float>();
		try {
			doc = Jsoup.connect("https://euw.op.gg/ranking/ajax2/champions.list/championId="+ championId.intValue()).get();
			
			// Les 5 premiers champions sont restitués différemment
			doc.select(".ranking-summary-summoner__item").forEach((element) ->{
				String winRateString = element.select(".ranking-summary-summoner__gamecount small").text().replace("%", "");
				if(summoners.size() < Constants.MAX_SUMMONER_SIZE) {
					summoners.put(element.select(".ranking-summary-summoner__name").text(), Float.parseFloat(winRateString.isEmpty() ? "0" : winRateString ));
				}
			});
			
			
			doc.select(".champion-ranking-table tr").forEach((element) ->{
				String winRateString = element.select(".champion-ranking-table__cell--winratio .winratio__text").text().replace("%", "");
				if(summoners.size() < Constants.MAX_SUMMONER_SIZE) {
					summoners.put(element.select(".champion-ranking-table__cell--summoner span").text(), Float.parseFloat(winRateString.isEmpty() ? "0" : winRateString));
				}
			});
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		System.out.println(summoners.entrySet().size() + " invocateurs trouvés : " + summoners.keySet());
		return summoners;
	}
}

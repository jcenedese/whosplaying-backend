package com.jcenedese.whosplaying;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jcenedese.whosplaying.dao.SchemaBuilder;

@SpringBootApplication
public class Main {
	
	@Autowired
	SchemaBuilder schemaBuilder;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        
        SchemaBuilder.createNewDatabase();
    }
}
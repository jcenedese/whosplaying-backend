package com.jcenedese.whosplaying.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jcenedese.whosplaying.service.LoLService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;


@RestController
@RequestMapping("/game")
@CrossOrigin(origins = "http://localhost:4200")
public class GameController {
	
	@Autowired
	private LoLService lolService;
	
	@ApiOperation(value = "Renvoie le fichier .bat permettant d'observer une partie")
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Fichier construit et retourné avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource") })
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/octet-stream" )
	public @ResponseBody byte[] getWatchFile(@ApiParam(required=true, allowEmptyValue=false) @RequestParam Long gameId,
			@ApiParam(required=true, allowEmptyValue=false) @RequestParam String platformId,
			@ApiParam(required=true, allowEmptyValue=false) @RequestParam String spectatorKey) {
		return this.lolService.getWatchFileForGameId(gameId, platformId, spectatorKey);
	}
}

package com.jcenedese.whosplaying.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jcenedese.whosplaying.model.PlayerDto;
import com.jcenedese.whosplaying.service.PlayerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponses;
import io.swagger.annotations.ApiResponse;
import net.rithms.riot.api.endpoints.summoner.dto.Summoner;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
@RequestMapping("/players")
public class PlayerController {
	
	@Autowired
	private PlayerService playerService;

	@ApiOperation(value = "Renvoie la liste de tous les joueurs", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Liste retournée avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource") })
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public List<PlayerDto> getAllPlayers() throws IllegalAccessException {
		throw new IllegalAccessException("Pas en core implémenté");
	}

	@ApiOperation(value = "Purge la base de données des joueurs")
	@RequestMapping(value = "", method = RequestMethod.DELETE)
	public void deletePlayers() throws IllegalAccessException {
		throw new IllegalAccessException("Pas en core implémenté");
	}
	
	@ApiOperation(value = "Purge la base de données des joueurs d'un champion")
	@RequestMapping(value = "/champions/{championId}", method = RequestMethod.DELETE)
	public void deletePlayersByChampionId(@PathVariable Integer championId) throws IllegalAccessException {
		throw new IllegalAccessException("Pas en core implémenté");
	}
	
	
	@ApiOperation(value = "Renvoie la liste des meilleurs joueurs d'un champion", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Liste retournée avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource"),
			@ApiResponse(code = 404, message = "Ressource non trouvée") })
	@RequestMapping(value = "/champions/{championId}", method = RequestMethod.GET, produces = "application/json")
	public List<PlayerDto> getPlayersByChampionId(@ApiParam(required=true, allowEmptyValue=false, example="131") @PathVariable Integer championId) {
		if(championId == null) {
			return new ArrayList<>();
		}
		return this.playerService.getBestPlayersByChampionId(championId.longValue());
	}

	@ApiOperation(value = "Renvoie les informations d'un joueur", response = PlayerDto.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Liste retournée avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource"),
			@ApiResponse(code = 404, message = "Ressource non trouvée") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public PlayerDto getPlayerById(@PathVariable Integer id) {
		return new PlayerDto(new Summoner(), 1L, new Float("0"));
	}
}
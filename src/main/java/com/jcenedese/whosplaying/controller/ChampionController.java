package com.jcenedese.whosplaying.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.jcenedese.whosplaying.model.ChampionDto;
import com.jcenedese.whosplaying.service.ChampionService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/champions")
@CrossOrigin(origins = "http://localhost:4200")
public class ChampionController {
	
	@Autowired
	ChampionService championService;
	
	@ApiOperation(value = "Renvoie les informations pour un champion donné", response = ChampionDto.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Champion retourné avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource") })
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ChampionDto getChampion(@ApiParam(required=true, allowEmptyValue=false, example="131") @PathVariable Integer id) {
		return this.championService.getChampion(id);
	}

	@ApiOperation(value = "Renvoie la liste de tous les champions du jeu", response = List.class)
	@ApiResponses(value = { @ApiResponse(code = 200, message = "Liste retournée avec succès"),
			@ApiResponse(code = 401, message = "Vous n'êtes pas autorisés à appeler cette ressource") })
	@RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
	public List<ChampionDto> getAllChampions() {
		return this.championService.getAllChampions();
	}
}

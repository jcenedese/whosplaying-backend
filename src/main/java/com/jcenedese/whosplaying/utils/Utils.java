package com.jcenedese.whosplaying.utils;

import java.io.File;
import java.io.FileInputStream;

public class Utils {
	/**
	 * Récupère une image du champion depuis son nom
	 * @param championName le nom du champion
	 * @return une tableau de byte
	 */
	public static byte[] getByteArrayFromFile(File file){  
		if(file == null){
			return null;
		}
        byte[] result=null;  
        FileInputStream fileInStr=null;  
        try{  
            fileInStr=new FileInputStream(file);  
            long imageSize=file.length();  
              
            if(imageSize>Integer.MAX_VALUE){  
                return null;
            }  
              
            if(imageSize>0){  
                result=new byte[(int)imageSize];  
                fileInStr.read(result);  
            }  
        }catch(Exception e){  
            e.printStackTrace();  
        } finally {  
            try {  
                fileInStr.close();  
            } catch (Exception e) {  
            }  
        }  
        return result;  
    }  
}

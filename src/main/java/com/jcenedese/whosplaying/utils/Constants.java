package com.jcenedese.whosplaying.utils;

import net.rithms.riot.constant.Platform;

public class Constants {
	public static final int MAX_SUMMONER_SIZE = 20;
	public static String DATABASE_URL = "jdbc:sqlite:whosplaying-backend.db";
	public static String API_KEY = "RGAPI-4cc65398-7283-4fdf-a6c7-2ae6d934fa0e";
	public static Platform GAME_SERVER = Platform.EUW;
	public static String OBSERVER_URL = "spectator.euw1.lol.riotgames.com:80";
}
